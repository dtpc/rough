import pytest_benchmark
import pytest
import numpy as np
from rough.glcm import tif_glcm_props, array_glcm_props
from rough.skglcm import glcm_tif, glcm_array
from pathlib import Path


array_params = {
    'arr': np.random.randint(0, 63, size=(6, 6), dtype=np.uint8),
    'properties': ['contrast'],
    'window_size': 3,
    'levels': 64,

}


def test_glcm_all():
    g = array_glcm_props(**array_params)
    skg = glcm_array(**array_params)

    np.testing.assert_array_equal(g, skg)


def test_glcm_array(benchmark):
    g = benchmark(array_glcm_props, **array_params)


def test_skglcm_array(benchmark):
    skg = benchmark(glcm_array, **array_params)


tif_params = {
    'tif': Path('./tests/resources/magnetics.tif'),
    'properties': ['contrast'],
    'window_size': 3,
    'levels': 256,
}


def test_glcm_tif(benchmark):
    benchmark(tif_glcm_props, **tif_params)
    benchmark(glcm_tif, **tif_params)


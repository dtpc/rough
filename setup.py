#!/usr/bin/env python

import versioneer
from setuptools import find_packages, setup

readme = open('README.rst').read()

with open('requirements.txt') as fh:
    requirements = [l.strip('\n') for l in fh]

setup(
    name='rough',
    version=versioneer.get_version(),
    description='Textural features',
    long_description=readme,
    author='CSIRO',
    author_email='david.cole@data61.csiro.au',
    url='https://bitbucket.csiro.au/projects/rose/repos/rough/browse',

    # Dependencies
    install_requires=requirements,
    extras_require={
        'dev': [
            'jupyter>=1.0.0',
            'mypy>=0.570',
            'versioneer>=0.18',
            'pytest>=3.1.3',
            'pytest-flake8>=0.8.1',
            'pytest-cov>=2.5.1',
            'flake8-docstrings>=1.1.0',
            'flake8-comprehensions>=1.4.1',
            'flake8-isort>=2.5',
        ]
    },
    dependency_links=[
    ],

    # Contents
    packages=find_packages(exclude=['test*']),
    package_data={'': ['*.pkl']},
    test_suite="tests",
    ext_modules=[],

    entry_points={
        'console_scripts': [
            'rough = rough.tools.cli:main',
        ]
    },

    # Other commands
    cmdclass=versioneer.get_cmdclass()
)

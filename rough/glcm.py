"""Extract texture properties from rasters."""

from itertools import product
from pathlib import Path
from typing import Iterable, List, NamedTuple, Optional

import numpy as np
import rasterio
from skimage.feature import greycoprops
from tqdm import tqdm

glcm_props = ('contrast', 'dissimilarity', 'homogeneity', 'energy',
              'correlation', 'ASM')


class Range(NamedTuple):
    start: int
    end: int


class Window(NamedTuple):
    rows: Range
    cols: Range


class Offset(NamedTuple):
    row: int
    col: int


def neighbour_offset(angle: float, distance: float) -> Offset:
    """Calcualte the neighbour pixel offset from angle and distance."""
    r_shift = int(round(np.sin(angle) * distance))
    c_shift = int(round(np.cos(angle) * distance))
    return Offset(r_shift, c_shift)


def sub_window_indexes(shift: Offset, window_size: int) -> Window:
    """Calculate the sub window indices given the neighbour offset."""
    rs, cs = shift
    r_start = abs(min(rs, 0))
    r_end = window_size - max(rs, 0)
    c_start = abs(min(cs, 0))
    c_end = window_size - max(cs, 0)
    return Window(Range(r_start, r_end), Range(c_start, c_end))


def apply_offset(window: Window, offset: Offset) -> Window:
    """Move a window by a given offset."""
    new_rows = Range(window.rows.start + offset.row,
                     window.rows.end + offset.row)
    new_cols = Range(window.cols.start + offset.col,
                     window.cols.end + offset.col)
    return Window(new_rows, new_cols)


def update_glcm(g_: np.ndarray,
                arr: np.ndarray,
                wa: Window,
                wb: Window,
                delta: int=1
                ) -> None:
    """Update the glcm matrix given the sub matrices."""
    a_ = arr[slice(*wa.rows), slice(*wa.cols)]
    b_ = arr[slice(*wb.rows), slice(*wb.cols)]
    for i, j in zip(np.nditer(a_), np.nditer(b_)):
        g_[i, j] += delta


def inc_update_glcm(g_: np.ndarray,
                    arr: np.ndarray,
                    win_a: Window,
                    win_b: Window,
                    is_col: int,
                    window_step=1
                    ) -> None:
    """Update the glcm matrix incrementally."""
    def _edge(w: Window, is_end: int) -> Window:
        """Calculate window edge."""
        e = w[is_col][is_end]
        r = Range(e - window_step, e)
        w = Window(w.rows, r) if is_col else Window(r, w.cols)
        return w

    edge_wa = _edge(win_a, is_end=0)
    edge_wb = _edge(win_b, is_end=0)
    update_glcm(g_, arr, edge_wa, edge_wb, delta=-1)
    edge_wa = _edge(win_a, is_end=1)
    edge_wb = _edge(win_b, is_end=1)
    update_glcm(g_, arr, edge_wa, edge_wb, delta=1)


def array_glcms(arr: np.ndarray,
                window_size: int,
                distances: List[float],
                angles: List[float],
                levels: int = 256,
                window_step: int = 1,
                symmetric: bool = False,
                normalise: bool = False,
                aggregate: bool = False
                ) -> Iterable[np.ndarray]:
    """Generate GLCM arrays over a sliding window on `arr`."""
    if window_size < 3 or window_size % 2 != 1:
        raise ValueError(f"Invalid window size: {window_size}")

    if window_step != 1:
        raise NotImplementedError("Only `window_step=1` is supported.")

    arr_max = arr.max()
    if arr_max > levels:
        msg = f"Max `arr` value ({arr_max})is greater than levels ({levels})."
        raise ValueError(msg)

    for d in distances:
        if not 1 <= d <= window_size - 1:
            msg = f"Distance ({d}) must be between 1 and (window_size - 1)."
            raise ValueError(msg)

    for t in angles:
        if not 0 <= t <= 2 * np.pi:
            msg = f"Angle ({t}) must be between 0 and 2pi."
            raise ValueError(msg)

    n_distances = len(distances)
    n_angles = len(angles)

    n_rows, n_cols = arr.shape
    halfwidth = (window_size - 1) // 2

    offsets = [[neighbour_offset(a, d) for a in angles] for d in distances]
    sub_windows = [[sub_window_indexes(ao, window_size) for ao in do]
                   for do in offsets]

    dtype = np.uint8 if levels <= 256 else np.uint16
    g = np.zeros((levels, levels, n_distances, n_angles), dtype=dtype)

    for r in range(0, n_rows - 2 * halfwidth, window_step):
        for c in range(0, n_cols - 2 * halfwidth, window_step):
            for d, t in np.ndindex((n_distances, n_angles)):
                offset = offsets[d][t]
                win_0 = sub_windows[d][t]
                win_a = apply_offset(win_0, Offset(r, c))
                win_b = apply_offset(win_a, offset)

                if c == 0:
                    if r == 0:
                        update_glcm(g[:, :, d, t], arr, win_a, win_b)
                    else:
                        inc_update_glcm(g[:, :, d, t], arr, win_a, win_b,
                                        is_col=0)
                    gc = g.copy()
                else:
                    inc_update_glcm(gc[:, :, d, t], arr, win_a, win_b, is_col=1)

            out = gc.copy()

            if symmetric:
                out += out.transpose(1, 0, 2, 3)

            if aggregate:
                out = out.sum(axis=(2, 3), keepdims=True)

            if normalise:
                out = out.astype(np.float32)
                out_sum = out.sum(axis=(0, 1))
                out_sum[out_sum == 0] = 1
                out /= out_sum

            yield out


def array_glcm_props(arr: np.ndarray,
                     properties: List[str],
                     window_size: int,
                     window_step: int=1,
                     distances: Optional[List[float]]=None,
                     angles: Optional[List[float]]=None,
                     levels: int=256,
                     symmetric: bool=False,
                     normalise: bool=False,
                     aggregate: bool=False,
                     show_progress: bool=False
                     ) -> np.ndarray:
    """Calculate glcm properties of an image `arr`."""
    # default to four angles and distance of 1.0
    angles = angles or [0, np.pi / 4, np.pi / 2, 3 * np.pi / 4]
    distances = distances or [1.0]

    glcms = array_glcms(arr, window_size=window_size, window_step=window_step,
                        distances=distances, angles=angles, levels=levels,
                        symmetric=symmetric, normalise=normalise,
                        aggregate=aggregate)

    nr, nc = arr.shape
    n_props = len(properties)
    n_neighbours = 1 if aggregate else len(distances) * len(angles)
    n_vals = n_props * n_neighbours
    res = np.full((n_vals, nr, nc), np.nan, dtype=np.float32)
    hw = (window_size - 1) // 2

    g_iter = product(range(hw, nr - hw), range(hw, nc - hw))
    if show_progress:
        total = (nr - 2 * hw) * (nc - 2 * hw)
        g_iter = tqdm(g_iter, total=total)

    for r, c in g_iter:
        g = next(glcms)
        vals = np.array([greycoprops(g, p) for p in properties])
        res[:, r, c] = vals.squeeze()

    return res


def tif_glcm_props(tif: np.ndarray,
                     properties: List[str],
                     window_size: int,
                     window_step: int = 1,
                     distances: Optional[List[float]] = None,
                     angles: Optional[List[float]] = None,
                     levels: int = 256,
                     symmetric: bool = True,
                     normalise: bool = True,
                     aggregate: bool = True,
                    perc: int=100,
                    out_dir: Optional[Path]=None,
                    separate_tifs: bool=False
                     ) -> List[Path]:
    """Generate texture tifs from input image."""
    base = tif.stem
    out_dir = out_dir or tif.parent

    with rasterio.open(str(tif), 'r') as src:
        meta = src.meta
        assert src.count == 1
        data = src.read(1)

    # bin data and convert to int
    c = (100 - perc) / 2
    l, u = np.percentile(data, (c, 100 - c))
    bins = np.linspace(l, u, levels)
    ddata = np.digitize(data, bins) - 1

    texts = array_glcm_props(ddata, properties=properties,
                             window_size=window_size, window_step=window_step,
                             distances=distances, angles=angles, levels=levels,
                             symmetric=symmetric, normalise=normalise,
                             aggregate=aggregate)

    n_textures = texts.shape[0]
    assert n_textures == len(properties)

    out = []

    def filename(label: str) -> str:
        """Generate output file name."""
        fn = f'{base}_{label}_win{window_size}_lvl{levels}'
        if perc != 100:
            fn += f'_perc{perc}'
        fn += f'_dist{len(distances)}_ang{len(angles)}'
        flags = ((symmetric, 'sym'), (normalise, 'norm'), (aggregate, 'agg'))
        for f, s in flags:
            if f:
                fn += f'_{s}'
        fn += '.tif'
        return fn

    if separate_tifs:
        meta['count'] = 1
        for i, p in enumerate(properties):
            out_name = filename(p)
            out_path = out_dir / out_name
            with rasterio.open(str(out_path), 'w', **meta) as dst:
                dst.write(texts[i, :, :], 1)
            out.append(out_path)
    else:
        meta['count'] = n_textures
        label = 'textures' if n_textures > 1 else properties[0]
        out_name = filename(label)
        out_path = out_dir / out_name
        with rasterio.open(str(out_path), 'w', **meta) as dst:
            dst.write(texts)
        out.append(out_path)

    return out

"""Extract texture properties from rasters using scikit-image."""

from pathlib import Path
from typing import List, Optional

import numpy as np
import rasterio
from skimage.feature import greycomatrix, greycoprops
from skimage.util import view_as_windows
from tqdm import tqdm


def glcm_window_props(w: np.ndarray,
                      props: List[str],
                      levels: int,
                      angles: Optional[List[float]]=None,
                      distances: Optional[List[int]]=None,
                      symmetric: bool=True,
                      normalise: bool=False,
                      aggregate: bool=True,
                      ) -> np.ndarray:
    """Calculate specified glcm texture properties on a given window `a`."""
    if not props:
        raise ValueError("Must specify a least one property.")


    glcm = greycomatrix(w, distances, angles, levels=levels, symmetric=symmetric)

    if aggregate:
        glcm = glcm.sum(axis=(2, 3), keepdims=True)

    if normalise:
        glcm = np.divide(glcm, glcm.sum(), dtype=np.float32)

    vals = np.array([greycoprops(glcm, p) for p in props])

    return vals


def glcm_array(arr: np.ndarray,
               properties: List[str],
               window_size: int,
               distances: Optional[List[float]]=None,
               angles: Optional[List[float]]=None,
               levels: int=256,
               symmetric: bool=False,
               normalise: bool=False,
               aggregate: bool=False,
               show_progress: bool=False
               ) -> np.ndarray:
    """Calculate glcm properties of an image."""
    nr, nc = arr.shape

    angles = angles or [0, np.pi / 4, np.pi / 2, 3 * np.pi / 4]
    distances = distances or [1]

    n_props = len(properties)
    n_neighbours = 1 if aggregate else len(distances) * len(angles)
    n_vals = n_props * n_neighbours

    out = np.full((n_vals, nr, nc), np.nan, dtype=np.float32)
    o = (window_size - 1) // 2
    ws = view_as_windows(arr, window_size, 1)

    g_iter = np.ndindex(nr - window_size, nc - window_size)
    if show_progress:
        total = (nr - window_size) * (nc - window_size)
        g_iter = tqdm(g_iter, total=total)

    for i, j in g_iter:
        out[:, i + o, j + o] = glcm_window_props(
            ws[i, j, ...], props=properties, levels=levels,
            angles=angles, distances=distances, symmetric=symmetric,
            aggregate=aggregate, normalise=normalise).squeeze()

    return out


def glcm_tif(tif: Path,
             properties: List[str],
             window_size: int,
             levels: int=256,
             perc: int=100,
             out_dir: Optional[Path]=None,
             separate_tifs: bool=False
             ) -> List[Path]:
    """Generate texture tifs from input image."""
    base = tif.stem
    out_dir = out_dir or tif.parent

    with rasterio.open(str(tif), 'r') as src:
        meta = src.meta
        assert src.count == 1
        data = src.read(1)

    # bin data and convert to int
    c = (100 - perc) / 2
    l, u = np.percentile(data, (c, 100 - c))
    bins = np.linspace(l, u, levels)
    ddata = np.digitize(data, bins) - 1
    print(data.shape)
    print(data.dtype)

    texts = glcm_array(ddata, n=window_size, props=properties, levels=levels)

    n_textures = texts.shape[0]
    assert n_textures == len(properties)

    if separate_tifs:
        meta['count'] = 1
        raise NotImplementedError()
    else:
        meta['count'] = n_textures
        label = 'textures' if n_textures > 1 else properties[0]
        out_name = f'{base}_t0_{label}_win{window_size}_lvl{levels}_sym.tif'
        out_path = out_dir / out_name
        with rasterio.open(str(out_path), 'w', **meta) as dst:
            #import IPython; IPython.embed()
            dst.write(texts)

    return [out_path]

"""Various utility functions for CLI tools."""

from pathlib import Path
from typing import List, Optional, Sequence

import click


def get_file_list(path: Path,
                  file_types: Sequence[str],
                  exists: bool=True
                  ) -> List[Path]:
    """If path is a directory return files within else return [path]."""
    path = path.expanduser()
    if exists and not path.exists():
        raise ValueError(f'Path `{path}` does not exist.')

    if path.is_dir():
        files = [p for t in file_types for p in path.rglob(f'*.{t}')]
    else:
        files = [path]
    return files


class StrList(click.ParamType):
    """A click param for single char separated lists."""

    name = "StrList"

    def __init__(self, sep: str=','):
        self.sep = sep

    def convert(self, value: str, param: Optional[click.Parameter],
                ctx: Optional[click.Context]) -> List[str]:
        """Convert char separated string into list."""
        return None if value is None else value.split(self.sep)


class ClickPath(click.ParamType):
    """A click param for any path which returns a pathlib.Path."""

    name = "Path"
    convert_fn = click.Path().convert

    def convert(self, value: str, param: Optional[click.Parameter],
                ctx: Optional[click.Context]) -> Path:
        """Convert path str to pathlib Path."""
        path = Path(self.convert_fn(value, param, ctx)).expanduser()
        return path


class ClickDir(ClickPath):
    """A click param for existing directory which returns a pathlib.Path."""

    name = "Dir"
    convert_fn = click.Path(exists=True, file_okay=False).convert


class ClickFile(ClickPath):
    """A click param for existing file which returns a pathlib.Path."""

    name = "File"
    convert_fn = click.Path(exists=True, dir_okay=False).convert

import click

from rough import __version__
from rough.tools.utils import get_file_list, ClickDir, ClickPath
from rough.glcm import glcm_props, tif_glcm_props
from rough.skglcm import glcm_tif


@click.group()
@click.version_option(version=__version__)
def main():
    """General utilities for geotifs, and shapefiles."""
    pass


@main.command()
@click.argument('infile', type=ClickPath(), required=True)
@click.option('--outdir', type=ClickDir(), required=False,
              help="Output directory (defaults to infile dir)")
@click.option('--window-size', type=int, help="Sliding window size.")
@click.option('--n-bins', type=int, default=256,
              help='Number of bins to use for GLCM.')
@click.option('--perc', type=click.IntRange(0, 100), required=False,
              default=100, help='Percent of data for binning to cover.')
@click.option('--properties', '-p', type=click.Choice(glcm_props),
              multiple=True, help="Texture properties.")
@click.option('--separate-tifs', is_flag=True, default=False,
              help="Save each texture separately.")
def textures(infile, outdir, window_size, n_bins, perc, properties,
             separate_tifs):
    """Generate textures from input image(s)."""
    infiles = get_file_list(infile, ('tif', 'gtif'))

    for gtif in infiles:
        print(f'Generating textures for {gtif}.')
        _outdir = str(outdir) if outdir else None
        out_paths = glcm_tif(gtif, props=properties, window_size=window_size,
                 n_bins=n_bins, perc=perc, out_dir=_outdir,
                 separate_tifs=separate_tifs)
        for o in out_paths:
            print(o)


@main.command()
@click.argument('infile', type=ClickPath(), required=True)
@click.option('--outdir', type=ClickDir(), required=False,
              help="Output directory (defaults to infile dir)")
@click.option('--window-size', type=int, help="Sliding window size.")
@click.option('--n-bins', type=int, default=256,
              help='Number of bins to use for GLCM.')
@click.option('--perc', type=click.IntRange(0, 100), required=False,
              default=100, help='Percent of data for binning to cover.')
@click.option('--properties', '-p', type=click.Choice(glcm_props),
              multiple=True, help="Texture properties.")
@click.option('--separate-tifs', is_flag=True, default=False,
              help="Save each texture separately.")
def textures2(infile, outdir, window_size, n_bins, perc, properties,
             separate_tifs):
    """Generate textures from input image(s)."""
    infiles = get_file_list(infile, ('tif', 'gtif'))

    for gtif in infiles:
        print(f'Generating textures for {gtif}.')
        _outdir = str(outdir) if outdir else None
        out_paths = tif_glcm_props(gtif, properties=properties, window_size=window_size,
                 levels=n_bins, perc=perc, out_dir=_outdir,
                 separate_tifs=separate_tifs)
        for o in out_paths:
            print(o)
